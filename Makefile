#
# NOTE: this Makefile should be copied or linked on level up,
# in your main NuttX project directory and not simply executed in place.
# This allows for clean separation of these Make rules and your project's
# repository. Furthermore, you can even add this repo as a submodule
# to your project.
#

include rules/rules.mk
