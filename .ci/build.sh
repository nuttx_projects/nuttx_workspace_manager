#!/bin/bash
set -ev

# Install prerequisites
apt update -y && apt install -y build-essential ccache git \
	lsb-release libz-dev genromfs
export PATH=/usr/lib/ccache:$PATH
export CCACHE_DIR=$(pwd)/ccache
rm -rf ccache

# Build kconfig-frontends (on eoan there's actually a package for this)

if [ "$(lsb_release -cs)" = "eoan" ]; then
  apt install -y kconfig-frontends
else
  apt install -y gperf libncurses5-dev flex bison dh-autoreconf wget pkg-config
  
  KCONFIG_FRONTENDS_VER=4.11.0.1
  wget https://bitbucket.org/nuttx/tools/downloads/kconfig-frontends-${KCONFIG_FRONTENDS_VER}.tar.bz2
  tar jxf kconfig-frontends-${KCONFIG_FRONTENDS_VER}.tar.bz2
  cd kconfig-frontends-${KCONFIG_FRONTENDS_VER}
  wget https://bitbucket.org/nuttx/tools/downloads/gperf3.1_kconfig_id_lookup.patch
  patch -p1 < gperf3.1_kconfig_id_lookup.patch

  autoreconf -fi
  ./configure --enable-mconf --disable-gconf --disable-qconf
  make
  make install # this will install to /usr/local
  ldconfig
  cd ..
fi

# Prepare workspace to test
files=$(echo *)
mkdir rules
mv $files rules/
ln -s rules/Makefile

# Test master (release should be tested against own release)
#make init TAG=nuttx-10.0.1

make init
make configure BOARDCONFIG=sim:nsh
make
