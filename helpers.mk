# 
# Some useful commands
#

define msg
        /bin/echo -e "\033[0;34m▐\033[0m "$1
endef

define err
	/bin/echo -e "\033[0;31m▐ ERROR:\033[0m "$1
endef

#
# Useful variables
#

EXTRA_APPS_FILES=$(foreach F,$(wildcard rules/files/extra_apps/*),$(notdir $(F)))
EXTRA_OS_FILES=$(foreach F,$(wildcard rules/files/extra_os/*),$(notdir $(F)))

# all files expect to be generated
EXTRA_APPS_TARGETS=$(foreach F,$(EXTRA_APPS_FILES),extra_apps/$(F)) apps/external 
EXTRA_OS_TARGETS=$(foreach F,$(EXTRA_OS_FILES),extra_os/$(F)) nuttx/external
