# NuttX Workspace Manager

The NuttX Workspace Manager is a set of make files which take care of initializing and organizing a project directory based on NuttX. The idea is to develop both applications, board and other OS components (such as drivers) fully out of NuttX tree so that it is possible to track mainline NuttX repos instead of maintaining a fork.

## Rationale

Typical use of NuttX involves either developing a custom board implementation (which can be a modification of an existing board), custom user applications or other OS components such as drivers, and any combination of the former. While in general one may want to work towards inclusion of this work towards mainline NuttX repositories, quite often this may not be the case. In this scenario, it is desireable to work on your own implementation and simply follow NuttX master (or release) branches, to benefit from continued updates, without having to maintain and work on a fork of the "nuttx" and apps "repos".

This framework is organized in such a way that both custom apps, board and extra OS components, can be self-contained in their own subdirectories. This implies that each of these could actually be maintained separately to a given project instance and included as a git submodule when needed. This maximizes reusability of these components.

Finally, this framework's set of scripts are self-contained in the same manner, so that it can be also included as a submodule of the complete project, which is assumed to be mantained in its own repository.

## Design decisions

The workspace manager is designed so that it should not obscure the handling of NuttX nor alter it in such a way that it feels ad-hoc or unfamiliar. For this reason, all commands issued by the top-level makefile are always printed in the console for transparency. In general, they are mostly mappings into NuttX buildsystem and scripts at `nuttx/`. In other cases, new make targets are defined for convenience, in order to avoid repetitive tasks and also having to go into `nuttx/` to run targets.

## Workspace Layout

To develop a particular NuttX-based project with this framework, the project workspace has the following structure:

    project/
    project/nuttx		# NuttX main repository (submodule)
    project/apps		# NuttX apps repository (submodule)
    project/extra_os		# Extra OS components
    project/extra_apps		# Extra applications
    project/board		# Optional: custom board
    project/boards/		# Optional alternative: directory containing multiple custom boards
    project/rules		# The contents of this repository (submodule)
    project/Makefile		# A symbolic link into project/rules/Makefile

In this case `project` is the top-level directory of the workspace and is assumed to be a git repository. 
 
Including the framework's repository as a submodule guarantees that even if this framework is updated, one's snapshot remains at a desired commit which is known to be working. The top-level Makefile will be the main interface employed by the user to perform all operations (going into nuttx/ is not required and not advised).

## How to use

### Initialization

To create a new project you should first initialize a new Git repository:

    $ mkdir project
    $ cd project
    $ git init

Then, add this repository as a submodule, under the path `rules`:

    $ git submodule add https://gitlab.com/nuttx_projects/nuttx_workspace_manager rules

Now link the included Makefile in the top-level directory:

    $ ln -s rules/Makefile

As a shortcut, you can add the following to your .bashrc:

    alias nxinit="git init && git submodule add git@gitlab.com:nuttx_projects/nuttx_workspace_manager rules && ln -s rules/Makefile"

and then setup the workspace by just doing:

    $ mkdir project
    $ cd project
    $ nxinit

Finally, initialize the workspace, which will create all directories with apropriate
contents and also add the nuttx/ and apps/ submodules:

    $ make init

During initialization, you can choose to add nuttx/apps submodules at a specific commit or tag:

    $ make init TAG=<tag or commit>

### Board selection

Right after initialization, you can simply choose to build NuttX with existing board definitions. Thus, as you would normally do, you need to first configure NuttX. To do so easily, simply run:

    $ make configure BOARDCONFIG=<board-config-entry>

where <board-config-entry> follows the expected argument to NuttX's `configure.sh` script. For example, to use the stm32f103-minimum:nsh configuration, you should run:

    $ make configure BOARDCONFIG=stm32f103-minimum:nsh

If you wish, can can also list all available boards by running:

    $ make list_boards

and also list all configurations of a given board with:

    $ make list_configs BOARD=<board>

where <board> is the board-name (eg. stm32f103-minimum).

### Configuration

You can enter menuconfig by simply doing:

    $ make menuconfig

This is just a convenience shorthand.

### Build

You can initiate build as usual by doing

    $ make

Targets not specific to this framework are passed on to nuttx/.

### Other targets

An extra target `depclean` is included, which erases all available Make.dep files.
This is important when one renames or moves source files, which usually results
in out-of-date dependency tracking. After `depclean` is invoked, the following
build, all dependency tracking files are regenerated (as is normally performed
in NuttX build system).

#### Compilation Database

Some IDEs, such as QtCreator (using an experimental plug-in you can enable), can open a project from a compilation database JSON file. This file contains a list of all your source files and the exact compiler flags used to build it. This is useful for opening a NuttX project in an IDE since otherwise you would have to manually add all relevant files to a project and specify correct include directories for code-completion to work.

To generate a compilation database for your NuttX project, simply do:

    make compile_commands.json

This will use `compiledb` to invoke `make`, but without actually executing any rules. This script can be installed with

    pip install compiledb

In QtCreator, you can (assuming you already enabled the plugin) open the json file, which will ask you for the root directory of your project and then you should be able to have a list of all your source files.

Note that you would need to regenerate the database whenever you modify any makefiles to add/remove source files.
