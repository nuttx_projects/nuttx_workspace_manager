help:
	@$(call msg, "Available commands:")
	@echo "  init           Initialize project (clone NuttX, create directories)"
	@echo "  configure      Select board and configuration"
	@echo "  list_boards    List all available boards"
	@echo "  list_configs   List all configs of given board"
	@echo "  menuconfig     Enter NuttX menuconfig"
	@echo "  clean          Clean NuttX and extra directories"
	@echo "  distclean      Distclean NuttX"
	@echo "  depclean       Remove all .depend/Make.dep files"
